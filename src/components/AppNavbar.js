import { Container } from "react-bootstrap";
import { Link } from "react-router-dom";
import navLogo from "../images/logo-white.png";
import { MdShoppingBag } from "react-icons/md";
import { HiMenu } from "react-icons/hi";
import { CgClose } from "react-icons/cg";
import UserContext from "../UserContext";
import { useContext } from "react";
import { motion } from "framer-motion";

const buttonVariants = {
  hover: {
    scale: 1.1,
    transition: {
      duration: 0.4,
      yoyo: Infinity,
    },
  },
};
// start
const AppNavbar = () => {
  const { user, unsetUser } = useContext(UserContext);

  const logout = () => {
    unsetUser();
  };

  const rightNav = !user.id ? (
    <>
      <Link to="/register">
        <motion.button
          className="btn common-btn px-3 me-3 register-btn"
          variants={buttonVariants}
          whileHover="hover"
        >
          Register
        </motion.button>
      </Link>
      <Link to="/login">
        <motion.button
          className="btn common-btn px-3 login-btn border-success"
          variants={buttonVariants}
          whileHover="hover"
        >
          Login
        </motion.button>
      </Link>
    </>
  ) : (
    <>
      {user.isAdmin ? (
        <Link className="nav-link text-white" to="/allOrders">
          Users Orders
        </Link>
      ) : (
        <Link className="nav-link text-white" to="/orders">
          Orders
        </Link>
      )}

      <Link className="nav-link text-white fs-4 nav-bag" to="/bag">
        <MdShoppingBag />
      </Link>

      <Link
        className="btn btn-danger common-btn px-3  logout-btn"
        onClick={logout}
        to="/login"
      >
        Logout
      </Link>
    </>
  );

  const menuToggler = () => {
    document.body.classList.toggle("open");
  };

  return (
    <>
      <div className="menu-toggle" onClick={menuToggler}>
        <HiMenu className="menu-btn d-lg-none" />
        <CgClose className="close-btn d-lg-none" />
      </div>
      <nav className="nav">
        <Container>
          <div className="navbar d-flex align-items-center justify-content-between">
            <img src={navLogo} alt="nav logo" className="nav-logo" />
            <div className="nav-items">
              <Link className="nav-link" to="/home">
                Home
              </Link>
              <Link className="nav-link" to="/products">
                Products
              </Link>
              {/* <Link className="nav-link" to="/">
                Contact
              </Link> */}
            </div>
            <div className="right-nav">{rightNav}</div>
          </div>
        </Container>
      </nav>
    </>
  );
};

export default AppNavbar;
