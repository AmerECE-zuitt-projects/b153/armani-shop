import { useContext } from "react";
import { Col, Card, OverlayTrigger, Tooltip } from "react-bootstrap";
import { Link } from "react-router-dom";
import { MdShoppingBag } from "react-icons/md";
import { BsFillInfoCircleFill } from "react-icons/bs";
import { BiLogInCircle } from "react-icons/bi";
import { useCart } from "react-use-cart";
import { toast } from "react-toastify";
import UserContext from "../UserContext";
import { motion } from "framer-motion";

const ProductCard = ({ productProp }) => {
  const { _id, imageUrl, title, description, price, category, quantity } =
    productProp;

  const { addItem } = useCart();
  const newItem = {
    id: productProp._id,
    productId: productProp._id,
    imageUrl: productProp.imageUrl,
    title: productProp.title,
    price: productProp.price,
    quantity: productProp.quantity,
  };

  const { user } = useContext(UserContext);

  const addToBag = () => {
    addItem(newItem);
    toast.success("Item Added to Bag", {
      position: "top-center",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      theme: "colored",
    });
  };

  const addToBagTooltip = (props) => (
    <Tooltip id="button-tooltip" {...props}>
      Add to Bag
    </Tooltip>
  );
  const loginTooltip = (props) => (
    <Tooltip id="button-tooltip" {...props}>
      Login to Order
    </Tooltip>
  );

  return (
    <Col lg="3" md="6">
      <Card className="product-card border-2">
        <Card.Img variant="top" src={imageUrl} className="product-img" />
        <Card.Body>
          <Card.Title>{title}</Card.Title>
          <Card.Text>Price: ${price}</Card.Text>
          <Link className="btn details-btn" to={`/products/${_id}`}>
            <BsFillInfoCircleFill className="mb-1" /> Details
          </Link>
        </Card.Body>
        {!user.id ? (
          <Link to="/login">
            <OverlayTrigger
              placement="right"
              delay={{ show: 250, hide: 400 }}
              overlay={loginTooltip}
            >
              <motion.button className="btn bag-btn" whileTap={{ scale: 0.8 }}>
                <BiLogInCircle />
              </motion.button>
            </OverlayTrigger>
          </Link>
        ) : (
          <OverlayTrigger
            placement="right"
            delay={{ show: 250, hide: 400 }}
            overlay={addToBagTooltip}
          >
            <motion.button
              className="btn bag-btn"
              onClick={addToBag}
              whileTap={{ scale: 0.8 }}
            >
              <MdShoppingBag />
            </motion.button>
          </OverlayTrigger>
        )}
      </Card>
    </Col>
  );
};

export default ProductCard;
